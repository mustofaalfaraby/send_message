const app = require("./src/main");
const { Logging } = require('./src/utils/logging.util')
require('dotenv').config()
var common = require('./src/utils/common.util')
var config = common.config();
const {MySQLDBConfig} = require('./src/utils/mysql.db.util')
const logger = require('./src/logger')

//set port
const port = process.env.PORT || config.port;

app.listen(port, async () => {
    Logging(`Initialize OrderManagement Service`)
    Logging(`Up and running! - - listening on port: ${port}`);
    Logging(`Running server on ${config.environment} Mode`)
    Logging(`VERSION: ${process.env.VERSION}`)
    // await initSecret();
    const { pool, startTransaction, commit, rollback } = require('./src/utils/database.util');
    global.pool = pool  
    global.startTransaction = startTransaction
    global.commit = commit
    global.rollback = rollback
});
  
  