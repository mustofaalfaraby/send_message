const testController = require('./controllers/test.controller')
const { celebrate, Joi, errors, Segments } = require('celebrate');
const Multer = require('multer')
const tokenChecker = require('./utils/tokenChecker.util')
const tokenCheckerStatic = require('./utils/tokenCheckerStatic')
const { registerSchema} = require('./models/requestchemas.model');
const multer = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 10 * 1024 * 1024, // Maximum file size is 10MB
  },
});

module.exports = function Routing(router) {
  const test = new testController.test;
  router.get("/", (req, res) => {
    res.status(200);
    res.send("Testing Services Connected");
  });

  router.post('/users', test.Insertdata);
  router.get('/users', test.getdata);
  router.delete('/users', test.Deletedata);

  router.post('/message', test.message);
  
  
  
}