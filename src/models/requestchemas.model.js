const { Joi } = require('celebrate');

module.exports.registerSchema = Joi.object().keys({
    1 :1,
    PRODUCT_TYPE_ID: Joi.string().required(),
    MEMBER_TYPE_ID: Joi.string().default('MEM0001'),
    // IS_NLE: Joi.string().required(),
    // COMPANY_NAME: Joi.string().required(),
    // COMPANY_NPWP: Joi.string().required(),
    // COMPANY_NPWP_FILE: Joi.string().required(),
    // PIC_NAME: Joi.string().required(),
    // PIC_MOBILE_PHONE: Joi.string().required(),
    // IS_MAIN_USER: Joi.string().required(),
    // OFFICE_PHONE: Joi.string().required(),
    // QTY_USER_REQUEST: Joi.string().required(),
    // USER_EMAIL: Joi.string().required(),
    // USER_PASSWORDS: Joi.string().required()
  });


module.exports.disclaimerSchema = Joi.object().keys({
    disclaimer_id: Joi.string().required()
});