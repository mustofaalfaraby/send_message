const crypto = require('crypto');
const axios = require('axios');
var common = require('../utils/common.util');
const { Logging, InsertLogging } = require('../utils/logging.util');
var config = common.config();
const { performance } = require('perf_hooks'); 
const { pool_bill, pool } = require('../utils/database.util');

class TestService {

    static InsertUsers(firstname, lastname, birthday, location){
        return new Promise(function(resolve, reject) {
            try {
                let sql = "insert into users (firstname, lastname, birthday, location) values (?,?,?,?)";
                pool.query(sql, [firstname, lastname, birthday, location], function (error, rows, fields) {
                    if (error) {
                        reject(false)
                    }
                    else {
                        resolve(true)
                    }
                })
            }
            catch (error) {
                reject(error)
            }
        })
    }


    static GetUsers(){
        return new Promise(function(resolve, reject) {
            try {
                let sql = "select id as id_user, firstname, lastname, birthday, location from users";
                pool.query(sql, function (error, rows, fields) {
                    if (error) {
                        reject(false)
                    }
                    else {
                        resolve(rows)
                    }
                })
            }
            catch (error) {
                reject(error)
            }
        })
    }


    static DeleteUsers(id_user){
        return new Promise(function(resolve, reject) {
            try {
                let sql = "delete from users where id = ?";
                pool.query(sql, [id_user], function (error, rows, fields) {
                    if (error) {
                        reject(false)
                    }
                    else {
                        resolve(true)
                    }
                })
            }
            catch (error) {
                reject(error)
            }
        })
    }


    static message(message, id_user){
        return new Promise(function(resolve, reject) {
            console.log('masuk sini')
            try {
                let sql = "insert into message (date_message, message, id_user) values (now(),?,?)";
                pool.query(sql, ['Happy birthday '+message, id_user], function (error, rows, fields) {
                    if (error) {
                        reject(false)
                    }
                    else {
                        resolve(true)
                    }
                })
            }
            catch (error) {
                reject(error)
            }
        })
    }


    static get_message(){
        return new Promise(function(resolve, reject) {
            try {
                let sql = "SELECT ss.date_message, CONCAT(u.firstname,' ',u.lastname) AS namee, ss.message FROM message ss "+
                           " INNER JOIN users u ON u.id = ss.id_user " +
                            " ORDER BY ss.id DESC LIMIT 1 ";
                pool.query(sql, function (error, rows, fields) {
                    if (error) {
                        reject(false)
                    }
                    else {
                        resolve(rows[0])
                    }
                })
            }
            catch (error) {
                reject(error)
            }
        })
    }
    
   
}

module.exports.testService = TestService