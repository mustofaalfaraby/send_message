
const express = require("express")
const bodyParser = require('body-parser')
// const config = require('./config/config.json')
var common = require('./utils/common.util')
var config = common.config();
var process = require('process');
const { escapeObject, unescapeObject } = require('./utils/validation.util.js')
const { Logging } = require('./utils/logging.util')
const { successResponse, errorCatch, errorResponse} = require ('./utils/response.util')
const { celebrate, Joi, errors, Segments } = require('celebrate');
const Routing = require('./routes.js')
const router = express.Router()
var helmet = require('helmet')

//creates express app
const app = express()
const { performance } = require('perf_hooks');

//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended:false}))

//parse application/json
app.use(bodyParser.json())

let whiteList = (origin) => {
    var whitelist = config.whitelist_domain;
    for (let a in whitelist) {
        if (origin == whitelist[a]) {
            return origin;
        }
    }
    if (whitelist.length == 0)
        return null;
    else
        return whitelist[0];
}

app.use((req, res, next) => {
    let origin = req.headers.origin;
    
    res.header("Access-Control-Allow-Origin", whiteList(origin));
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, x-access-token, X-Requested-With, Content-Type, Accept, Authorization, Proxy-Authorization, X-session, Pragma, Cache-control, Expires");
    res.header("Access-Control-Expose-Headers", "Content-Disposition");
    res.header("Access-Control-Allow-Methods", "GET, PUT, DELETE, POST");
    res.header("X-XSS-Protection", "1");
    res.header("X-Content-Type-Options", "nosniff");
    res.header("Content-Security-Policy", "object-src 'none';img-src 'self';media-src 'self';frame-src 'none';font-src 'self' data:;connect-src 'self';style-src 'self'");
    res.header("Content-Type","application/json");
    res.header("Content-Length", 10000);
    if(req.path != '/api/v1/admin/server/status'){
        Logging('==============================================');
        Logging('Origin request from: ', origin);
        Logging('incoming request host : ' + req.headers.host);
        Logging('Incoming request method : ' + req.method);
        Logging('Incoming request path : ' + req.path);

        req.start_time = performance.now();
        req.client = {
            CLIENT_ID: null,
            CLIENT_CODE: null,
            CLIENT_NAME: null,
            CLIENT_TOKEN: null
        }

    }
   
    
    if (req.method === 'POST') {
   
        if(req.path != '/api/v4/login' || req.path != '/api/v4/register'){
            Logging("Body : " + JSON.stringify(req.body));
        }
    }

    next();
});

//set router
Routing(router)


//disable x-powered
// app.disable('x-powered-by')

//use helmet
app.use(helmet())

//set endpoint
app.use('/api/', router);

//JOI Error Handling
app.use((error, req, res, next) => {
    if (error.joi) {
        errorResponse(res,400,16009,null,error.joi.message, req)
    }
    else errorCatch(res,error, req)
  });
module.exports = app;
