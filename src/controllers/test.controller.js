
const { testService } = require('../services/test.service')
const { Logging } = require('../utils/logging.util')
const { successResponse, errorCatch, errorResponse } = require('../utils/response.util')
var jwt = require('jsonwebtoken')
var base64 = require("base-64");
const { v4: uuidv4 } = require('uuid');
const uploadFile = require('../utils/uploadFile.util')
var common = require('../utils/common.util')
const { pool } = require('../utils/database.util')
var config = common.config();
const AdmZip = require("adm-zip");
const { query } = require('express')

class TestController {

  async Insertdata(req, res, next) {
    try {   
      testService.InsertUsers(req.body.firstname, req.body.lastname, req.body.birthday, req.body.location).then(result => {
        var response = {
          "status": 1,
          "message": "Success",
          "data": req.body
        }
        successResponse(res, 200, response, req);
      }, error => {
        if (error.httpCode)
          errorResponse(res, error.httpCode, error.errorCode, '', '', req);
        else
          errorCatch(res, error, req);
      })

    } catch (error) {
      console.error('Catch error at @test.controller/Insertdata => ', error)
      errorCatch(res, error, req)
    }
  }

  async getdata(req, res, next) {
    try {   
      testService.GetUsers().then(result => {
        var response = {
          "status": 1,
          "message": "Success",
          "data": result
        }
        successResponse(res, 200, response, req);
      }, error => {
        if (error.httpCode)
          errorResponse(res, error.httpCode, error.errorCode, '', '', req);
        else
          errorCatch(res, error, req);
      })

    } catch (error) {
      console.error('Catch error at @test.controller/getdata => ', error)
      errorCatch(res, error, req)
    }
  }

  async Deletedata(req, res, next) {
    try {   
      testService.DeleteUsers(req.body.id_user).then(result => {
        var response = {
          "status": 1,
          "message": "Success",
          "data": req.body
        }
        successResponse(res, 200, response, req);
      }, error => {
        if (error.httpCode)
          errorResponse(res, error.httpCode, error.errorCode, '', '', req);
        else
          errorCatch(res, error, req);
      })

    } catch (error) {
      console.error('Catch error at @test.controller/Deletedata => ', error)
      errorCatch(res, error, req)
    }
  }

  async message(req, res, next) {
    try {   
       await testService.message(req.body.email, req.body.id_user).then(async result => {
        let data = await testService.get_message()
        data.message = "Hey, "+data.namee+" it’s your birthday."
        var response = {
          "status": 1,
          "message": "Success",
          "data": data
        }
        
        
        successResponse(res, 200, response, req);

        // const axios = require('axios');
        //   let datas = JSON.stringify({
        //     "email"  : req.body.email,
        //     "message": "Hey, "+data.namee+" it’s your birthday." 
        //   });

        //   let config = {
        //     method: 'post',
        //     maxBodyLength: Infinity,
        //     url: 'https://email-service.digitalenvision.com.au/send-email',
        //     headers: { 
        //       'Content-Type': 'application/json'
        //     },
        //     data : datas
        //   };

        //   axios.request(config)
        //   .then((response) => {
        //     console.log(JSON.stringify(response.data));
        //   })
        //   .catch((error) => {
        //     console.log(error);
        //   });


      }, error => {
        if (error.httpCode)
          errorResponse(res, error.httpCode, error.errorCode, '', '', req);
        else
          errorCatch(res, error, req);
      })

    } catch (error) {
      console.error('Catch error at @test.controller/message => ', error)
      errorCatch(res, error, req)
    }
  }

}

module.exports.test = TestController