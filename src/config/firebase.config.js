

var env = require('./firebase.auth.json');

exports.firebaseConfig = function() {
  var node_env = process.env.NODE_ENV || 'DEV';
  return env[node_env];
};

// module.exports.firebaseConfig = firebaseConfig