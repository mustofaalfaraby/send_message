const {Storage} = require('@google-cloud/storage');
var common = require('./common.util')
var config = common.config();

const keyFile = './src/config/cloudstorage.serviceaccount.json'

const DEFAULT_BUCKET_NAME = config.DEFAULT_BUCKET_NAME;

const GOOGLE_CLOUD_PROJECT_ID = config.GOOGLE_CLOUD_PROJECT_ID;
const GOOGLE_CLOUD_KEYFILE = keyFile;

// const storage = new Storage({
//   projectId: GOOGLE_CLOUD_PROJECT_ID,
//   keyFilename: GOOGLE_CLOUD_KEYFILE,
// });

const storage = new Storage();

class UploaderUtil {
  
    static getPublicUrl (bucketName, destination){ return `https://storage.googleapis.com/${bucketName}/${destination}`};

    static async uploadToGCP(p_file, p_destination){
        return new Promise(function(resolve , reject ){
            try {
              const bucketName = DEFAULT_BUCKET_NAME;
              const bucket = storage.bucket(bucketName);
              const gcsFileName = `${Date.now()}-${p_destination}`;
              const file = bucket.file(p_destination);
              
              const stream = file.createWriteStream({
                public: false,
                gzip: true,
                metadata: {
                  contentType: p_file.mimetype,
                  'Cache-Control':'no-cache, maxAge:0',
                }
              });
            
              const CONFIG = {
                action: "read",
                expires: "03-01-3000",
              };
            
              stream.on('error', (err) => {
                p_file.cloudStorageError = err;
                reject(err);
              });
            
              stream.on('finish', () => {
                p_file.cloudStorageObject = p_destination;
                file.getSignedUrl(CONFIG,function(err, url) {
                  if (err) {
                      p_file.cloudStorageError = err;
                      reject(err);
                  }else {
                      p_file.gcsUrl = url
                      resolve(url)
                  }
                })
              });
              stream.end(p_file.buffer); 
            } catch (error) {
              console.log('Error upload.utils:',error)
                reject(error)
            }
        });
    }
}

module.exports.UploaderUtil = UploaderUtil

