var admin = require('firebase-admin');
const firebaseConfig = require('../config/firebase.config.js')
var serviceAccount = require("../config/firebase.serviceaccount.json");
const {Logging} = require('./logging.util')

module.exports = async function send_to_device(p_registration_token,p_msg,p_options) {
  return new Promise((resolve, reject)=>{
    try {
      if(!admin.apps.length){
        admin.initializeApp({
          credential: admin.credential.cert(serviceAccount),
          databaseURL: firebaseConfig.databaseURL
        });
    }
    
    const options = {
      priority: "high",
      // timeToLive: 20000
  }

    // Send a message to the device corresponding to the provided
    // registration token.
    admin.messaging().sendToDevice(p_registration_token,p_msg,p_options)
      .then((response) => {
        // Response is a message ID string.
        
        console.log('Successfully sent message:', response);
        resolve(true)
      })
      .catch((error) => {
        
        console.log('Error sending message:', error);
        resolve(true)
      });
      
    } catch (error) {
      resolve(true)
    }
    
  });
}