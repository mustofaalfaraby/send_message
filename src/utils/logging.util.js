var moment = require('moment');
var common = require('./common.util')
var config = common.config();

module.exports = {
    Logging:function (msg) {
        if (config.debug) {
            var time = moment().format('DD-MMM-YYYY, hh:mm:ss a');
            console.log(`${time} | ${Object.prototype.toString.call(msg) == "[object Object]" ||
            Object.prototype.toString.call(msg) == "[object Array]" ? JSON.stringify(msg) : msg}` );
        }
    },

    ErrorLogging:function(msg) {
        if (config.debug) {
            var time = moment().format('DD-MMM-YYYY, hh:mm:ss a');
            console.error(`${time} | ${Object.prototype.toString.call(msg) == "[object Object]" ||
            Object.prototype.toString.call(msg) == "[object Array]" ? JSON.stringify(msg) : msg}` );
        }
    },
    WarnLogging: function(msg) {
        if (config.debug) {
            var time = moment().format('DD-MMM-YYYY, hh:mm:ss a');
            console.warn(`${time} | ${Object.prototype.toString.call(msg) == "[object Object]" ||
            Object.prototype.toString.call(msg) == "[object Array]" ? JSON.stringify(msg) : msg}` );
        }
    },
    InsertLogging: function(p_data, req){
        try {
            var query = 'Call prc_insert_tx_log_ordermanagement(?,?,?,?,?,?,?,?,?)';
            pool.query(query,[ p_data.syscode,
                                p_data.client_id,
                                p_data.process,
                                p_data.url,
                                p_data.req_header,
                                p_data.req_body,
                                p_data.response,
                                p_data.response_status,
                                p_data.cost_time], function (error, rows, fields){
            });
        } catch (error) {
            errorCatch(res,error)
        }          
    }

}