require('dotenv').config()
const util = require('util')
const mysql = require('mysql')
var common = require('./common.util')
var config = common.config();
const {MySQLDBConfig} = require('./mysql.db.util')

const pool = mysql.createPool(MySQLDBConfig.getConnectionDetails())


  // Ping database to check for common exception errors.
  pool.getConnection((err, connection) => {
    if (err) {
      if (err.code === 'PROTOCOL_CONNECTION_LOST') {
        console.error('Database connection was closed.')
      }
      if (err.code === 'ER_CON_COUNT_ERROR') {
        console.error('Database has too many connections.')
      }
      if (err.code === 'ECONNREFUSED') {
        console.error('Database connection was refused.')
      }
    }

    if (connection) connection.release()

    return
  })

  


  startTransaction = () => {
    return new Promise(function(resolve , reject ){
      try {
        pool.getConnection((err, connection) => {
          if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
              console.error('Database connection was closed.')
            }
            if (err.code === 'ER_CON_COUNT_ERROR') {
              console.error('Database has too many connections.')
            }
            if (err.code === 'ECONNREFUSED') {
              console.error('Database connection was refused.')
            }
            reject(err)
          }
      
          if (connection){
            connection.beginTransaction(function(err) {
              if(err){
                  console.error(error)
                  reject(error)
              }
              else
                  resolve(connection)
            });
          }
        })

        

      } catch (error) {
        reject(error)
      }
    })
  }

  

  commit = (p_con) => {
      return new Promise(function(resolve , reject ){
        try {
          p_con.commit(function(err) {
            if (err) { 
              p_con.rollback(function() {
                    p_con.release()
                    if(err.code==='ER_NO_REFERENCED_ROW_2'){
                        reject({
                            httpCode : 400,
                            errorCode : 16009
                        })
                    }else
                        reject(err);
                });
              }else{
                  p_con.release()
                  resolve(true)
              }
          }); 
        } catch (error) {
            console.error('Error Commit:', error)
            reject(error)
          }
      });
  };

  rollback = (p_con) => {
    return new Promise(function(resolve , reject ){
      try {
          p_con.rollback(function() {
              console.warn("Rollback Transaction")
              p_con.release()
              resolve(true)
          }); 
      } catch (error) {
          console.error('Error Rollback Util:', error)
          reject(error)
        }
    });
};
  // Promisify for Node.js async/await.
  pool.query = util.promisify(pool.query)

module.exports.pool = pool
module.exports.startTransaction = startTransaction
module.exports.commit = commit
module.exports.rollback = rollback
