require('dotenv').config()
const { Storage } = require('@google-cloud/storage');
var common = require('./common.util')
var config = common.config();

const keyFile = './src/config/cloudstorage.serviceaccount.json'
const { Logging }  = require ('./logging.util')
const DEFAULT_BUCKET_NAME = config.DEFAULT_BUCKET_NAME;

const GOOGLE_CLOUD_PROJECT_ID = process.env.GOOGLE_CLOUD_PROJECT_ID || config.GOOGLE_CLOUD_PROJECT_ID;
const GOOGLE_CLOUD_KEYFILE = keyFile;


//FOR DEV
// const storage = new Storage({
//   projectId: GOOGLE_CLOUD_PROJECT_ID,
//   keyFilename: GOOGLE_CLOUD_KEYFILE,
// });

//FOR UAT
const storage = new Storage(GOOGLE_CLOUD_PROJECT_ID);

class GoogleCloudStorage {
  
    static async uploadToGCP(p_file, p_destination, p_bucket_name){
        return new Promise(function(resolve , reject ){
            try {
                if(p_file){
                    console.log("Uploading file to: ",p_destination)
                    const bucketName = p_bucket_name || DEFAULT_BUCKET_NAME;
                    const bucket = storage.bucket(bucketName);
                    const file = bucket.file(p_destination);
                    const stream = file.createWriteStream({
                        public: false,
                        gzip: true,
                        metadata: {
                            contentType: p_file.mimetype,
                            'Cache-Control':'no-cache, maxAge:0',
                        }
                    });
      
                    stream.on('error', (err) => {
                        console.error('Error upload to Google Cloud => ', err)
                        return reject(err);
                    });
              
                    stream.on('finish', () => {
                        GoogleCloudStorage.getSignedUrl(p_destination).then(result=>{
                            return resolve({
                                file_path: p_destination,
                                url : result,
                                original_name : p_file.originalname
                            })
                        },error=>{
                            console.error('Error upload to Google Cloud => ', error)
                            reject(error)
                        })
                    });
                    
                    stream.end(p_file.buffer); 
                }else{
                    return resolve({
                        file_path: null,
                        url : null,
                        original_name : null
                    })
                }
              

            }catch (error) {
                console.error('Error upload to Google Cloud => ', error)
                reject(error)
            }
        });
    }

    static async uploadToGCPfromLocal(p_file, p_destination, p_bucket_name){
        return new Promise(function(resolve , reject ){
            try {
                if(p_file){
                    console.log("Uploading file to: ",p_destination)
                    const bucketName = p_bucket_name || DEFAULT_BUCKET_NAME;
                    storage.bucket(bucketName).upload(p_file, {
                        public: false,
                        // Support for HTTP requests made with `Accept-Encoding: gzip`
                        gzip: true,
                        // By setting the option `destination`, you can change the name of the
                        // object you are uploading to a bucket.
                        metadata: {
                          // Enable long-lived HTTP caching headers
                          // Use only if the contents of the file will never change
                          // (If the contents will change, use cacheControl: 'no-cache')
                          cacheControl: 'no-cache, maxAge:0',
                        },
                      }).then(result => {
                        GoogleCloudStorage.getSignedUrl(p_destination, undefined ,p_bucket_name).then(url=>{
                            return resolve({
                                file_path: p_destination,
                                url : url
                            })
                        },error=>{
                            console.error('Error upload to Google Cloud => ', error)
                            reject(error)
                        })
                      });
                  
                }else{
                    return resolve({
                        file_path: null,
                        url : null
                    })
                }
              
            }catch (error) {
                console.error('Error upload to Google Cloud => ', error)
                reject(error)
            }
        });
    }

    static async getSignedUrl(p_destination, p_config, p_bucket_name){
        return new Promise(function(resolve , reject ){
            try {
              if(p_destination){
                const bucketName = p_bucket_name || DEFAULT_BUCKET_NAME;
                const bucket = storage.bucket(bucketName);
                const file = bucket.file(p_destination);
                let CONFIG;
                if(!p_config){
                  CONFIG = {
                    // version: 'v4',
                    action: 'read',
                    expires: Date.now()+15*60*1000, //15 minutes,
                  };
                }else{
                  CONFIG = p_config;
                }
                file.getSignedUrl(CONFIG,function(err, url) {
                    if (err) {
                        reject(err);
                    }else {
                        Logging("getSignedUrl: "+url);
                        resolve(url)
                    }
                  })
                  
              }else{
                resolve(null)
              }
            } catch (error) {
                reject(error)
            }
        });
    }
}

module.exports.GoogleCloudStorage = GoogleCloudStorage

