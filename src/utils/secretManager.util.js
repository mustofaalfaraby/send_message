const { SecretManagerServiceClient } = require('@google-cloud/secret-manager');
const  { Logging }  = require ('./logging.util')
var common = require('./common.util')
var config = common.config();

const client = new SecretManagerServiceClient();

class SecretManager {
  
    static async getSecret(keyName){
            try {
                const projectId = process.env.SECRET_PROJECT_ID || config.secret_project_id;
                // const client = new SecretManagerServiceClient();

                const name = `projects/${projectId}/secrets/${keyName}/versions/latest`;


                Logging("getting secret - path: " + name);

                const [ secret ] = await client.accessSecretVersion({

                name

                });
                
                const keySecretValue = secret.payload.data.toString("utf8") || "";
                // console.debug("keySecretValue: ", keySecretValue)
                Logging(`Successfully get secret of ${keyName}`);

                return keySecretValue;
            }catch (error) {
                console.error(error)
                return(error)
            }
    }
}

module.exports.SecretManager = SecretManager

