const {SecretManager} = require('./secretManager.util')
var common = require('./common.util')
var config = common.config();

if (config.environment == 'Development'){
    class MySQLDBConfig {
      static MYSQL_DB = {
          connectionLimit:  config.connection_limit || 2,
          host: "localhost",
          user: "root",
          database: "test",
          port: 3306,
          password: "",
          typeCast: function castField( field, useDefaultTypeCasting ) {
            if ( ( field.type === "BIT" ) && ( field.length === 1 ) ) {
              var bytes = field.buffer();
              var ret;
              if(bytes!=null)
              ret = bytes[ 0 ] === 1  
              else ret = null
              return(ret );
            }
            return( useDefaultTypeCasting() );
          }
      };

      static getConnectionDetails() {
              return this.MYSQL_DB;
      }

      static async setPasswordSecret() {
          const pass = await SecretManager.getSecret( process.env.DB_PASSWORD || "undefined");
          this.MYSQL_DB.password = pass.trim() || process.env.DB_PASSWORD || "";
      }
      
  }
  exports.MySQLDBConfig = MySQLDBConfig;
}





