const firebaseConfig = require('../config/firebase.config.js')
const serviceAccount = require("../config/firebase.serviceaccount.json");
const { errorCatch, errorResponse, errorResponseNew } = require ('./response.util')
const admin = require('firebase-admin');
// const { getAuth } = require ('firebase-admin/lib/auth');
const jwt = require('jsonwebtoken');
const NodeRSA = require('node-rsa');




/**
 * Middleware to check/validate token
 */

module.exports = (req,res,next) => {
  try {
    let checkRevoked = true;
    const { authorization } = req.headers;
    if (!authorization || authorization == undefined) {
      errorResponse(res, 401, 16008);
    } else {
      const [authType, token] = authorization.trim().split(" ");
      if (authType !== "Bearer") errorResponseNew(res, 401, 16007,'','',req);
      // console.log('token nya bray :',config.token.logol)
      return jwt.verify(token, "logol_selfservice", function (err, decoded) {
        if (err == null) {
            req.decode = decoded.claims;
            next();
        } else {
            if (err.message === 'jwt expired') {
              res.status(400).json({
                  status: 0,
                  message : "Failed",
                  data : "Token is invalid!"
              })
              errorResponseNew(res,400,'Token is invalid','','',req);
            } else if (err.message !== 'jwt expired') {
                  res.status(400).json({
                    status: 0,
                    message : "Failed",
                    data : "Token is invalid!"
                })
                errorResponseNew(res,400,'Token is invalid','','',req);
            }
        }
      });
    }
    
  // try {
    

  //   let checkRevoked = true;
  //   const { authorization } = req.headers
  //   if (!authorization || authorization==undefined) {errorResponse(res,401,16008)}
  //   else{
  //     const [authType, token] = authorization.trim().split(' ')
  //     if (authType !== 'Bearer') errorResponse(res,401,16007)
    
  //     // if(!admin.apps.length){
  //     //     admin.initializeApp();
  //     // }


  //     var admin = require("firebase-admin");
  //     var serviceAccount = require('../config/logistic-online-v4-firebase-adminsdk-8tpw9-c6bb3c468f.json');
  //     if(!admin.apps.length){
  //         var t_admin_app = admin.initializeApp({
  //             credential: admin.credential.cert(serviceAccount)
  //         });
  //     }

      
  //     // const tokenString = req.headers['authorization'] ? req.headers['authorization'].split(" ") : null;
  //     // console.log('tokenString = >', tokenString)       
  //     // getAuth().verifyIdToken(token || 'invalid',checkRevoked)
  //     // .then((decodedToken) => {
  //     //   console.log('decodedToken =>',decodedToken)
  //     //   // const uid = decodedToken.uid;
  //     //   res.status(200).json({
  //     //               status: 0,
  //     //               message: "Token is valid"
  //     //           })
  //     // })
  //     // .catch((error) => {
  //     //   console.log('error =>', error);
  //     //   res.status(400).json({
  //     //               status: 0,
  //     //               message: "Token invalid"
  //     //           })
  //     // });

  //     // t_admin_app.auth().verifyIdToken(token || 'invalid',checkRevoked)
  //     //         .then(function(decodedToken) {
  //     //             req.decoded = decodedToken;
  //     //             next();
  //     //         }).catch(function(error) {
  //     //             console.error(error)
  //     //             if (error.code === 'auth/id-token-revoked') {
  //     //                // Token has been revoked. Inform the user to reauthenticate.
  //     //                errorResponse(res,401,16005)
  //     //             } else if (error.code ==='auth/id-token-expired') {
  //     //                // Token has been expired. Inform the user to reauthenticate or signOut() the user.
  //     //                errorResponse(res,401,16002)
  //     //             }else{// Token is invalid.
  //     //                errorResponse(res,401,16006)
  //     //             }
  //     //         })
        

  //         const publicKey = new NodeRSA().importKey(serviceAccount.private_key, "pkcs8-private-pem").exportKey("pkcs8-public-pem")
  //         jwt.verify(token, publicKey, {
  //                 algorithms: ["RS256"]
  //             }, (err, decoded) => {
  //                 if (err) {
  //                     // send some error response
  //                   //   res.status(400).json({
  //                   //       status: 0,
  //                   //       message : "Failed",
  //                   //       data : "Token is invalid!"
  //                   //   })
  //                   errorResponseNew(res,400,'Token is invalid','','',req);
  //                 } else {
  //                     req.decode = decoded.claims;
  //                     next();
  //                 }
  //           }) 

  //     }  



  } catch (error) {
    errorCatch(res,error,req)
  }
}
