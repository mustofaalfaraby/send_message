const { Logging, WarnLogging, ErrorLogging, InsertLogging } = require ('./logging.util');

const errorCodes = require('../config/error.json');
const { json } = require('body-parser');
const { performance } = require('perf_hooks');

module.exports = {
    successResponse: function(resObject, statusCode, data, req) {
        
        // let p_data = {
        //     syscode : 'LogolServices',
        //     client_id : req.client.CLIENT_ID == undefined ? null : req.client.CLIENT_ID,
        //     process : null,
        //     url : req.headers.host+'/api/v4'+req.path,
        //     req_header : req.query == undefined ? null : JSON.stringify(req.query),
        //     req_body : req.body == undefined ? null : JSON.stringify(req.body),
        //     response : JSON.stringify(data),
        //     response_status : statusCode,
        //     cost_time : (performance.now() - req.start_time) / 1000
        // }
        // InsertLogging(p_data);
        Logging("Data : " + JSON.stringify(data));
        if(data) {
            resObject.status(statusCode).json(data);
        } else {
            resObject.status(statusCode).json();
        }
    },
    errorResponse: function(resObject, statusCode, errorCode, custmessage, log, req) {
        let errorDesc
        if(errorCodes[errorCode]) {
            errorDesc = errorCodes[errorCode];
        }else if (custmessage){
            errorDesc = custmessage
        }else{
            errorDesc = "[unspesified error]";
        }

        let p_data = {
            syscode : 'LogolServices',
            client_id : req.client.CLIENT_ID == undefined ? null : req.client.CLIENT_ID,
            process : null,
            url : req.headers.host+'/api/v4'+req.path,
            req_header : req.query == undefined ? null : JSON.stringify(req.query),
            req_body : req.body == undefined ? null : JSON.stringify(req.body),
            response : JSON.stringify({ status: 0, message : 'Failed', data : errorDesc+' '+ (log ? log : "")}),
            response_status : statusCode,
            cost_time : (performance.now() - req.start_time) / 1000,
        }
        InsertLogging(p_data);
        WarnLogging('['+errorCode+'] Error: '+errorDesc+' '+ (log ? log : ""))
        resObject.status(statusCode).json({
            status: 0,
            message : 'Failed',
            data : errorDesc+' '+ (log ? log : "")
        });
    },
    errorResponseNew: function(resObject, statusCode, errorCode, custmessage, log,req) {
        let errorDesc
        let p_data = {
            syscode : 'LogolServices',
            client_id : req.client.CLIENT_ID == undefined ? null : req.client.CLIENT_ID,
            process : null,
            url : req.headers.host+'/api/v4'+req.path,
            req_header : req.query == undefined ? null : JSON.stringify(req.query),
            req_body : req.body == undefined ? null : JSON.stringify(req.body),
            response : JSON.stringify({ status: 0, message : 'Failed', data : errorCode}),
            response_status : statusCode,
            cost_time : (performance.now() - req.start_time) / 1000,
        }
        InsertLogging(p_data);
        resObject.status(statusCode).json({
            status: 0,
            message : 'Failed',
            data : errorCode
        });
    },

    errorCatch: function(resObject, error,req ) {
        let errorDesc
        let errorCode = '99999'
        let logMessage
        let statusCode = "501"

        if(errorCodes[errorCode]) {
            errorDesc = errorCodes[errorCode];
        }else if (custmessage){
            errorDesc = custmessage
        }else{
            errorDesc = "[unspesified error]";
        }
                
        // let p_data = {
        //     syscode : 'LogolServices',
        //     client_id : req.client.CLIENT_ID == undefined ? null : req.client.CLIENT_ID,
        //     process : null,
        //     url : req.headers.host+'/api/v4'+req.path,
        //     req_header : req.query == undefined ? null : JSON.stringify(req.query),
        //     req_body : req.body == undefined ? null : JSON.stringify(req.body),
        //     response : JSON.stringify({ status: 0, message : 'Failed', data : (error.message || JSON.stringify(error) || 'Unknown Error' )}),
        //     response_status : statusCode,
        //     cost_time : (performance.now() - req.start_time) / 1000
        // }
        // // console.log(p_data);
        // InsertLogging(p_data);
        ErrorLogging('['+errorCode+'] Critical Error: '+ (error.message || JSON.stringify(error) || 'Unknown Error' ))
        
        resObject.status(statusCode).json({
            status: 0,
            message : 'Failed',
            data : (error.message || JSON.stringify(error) || 'Unknown Error' )
        });

    },
};
