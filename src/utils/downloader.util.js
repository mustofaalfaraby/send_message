const {Storage} = require('@google-cloud/storage');
var common = require('./common.util')
var config = common.config();

const keyFile = './src/config/cloudstorage.serviceaccount.json'

const DEFAULT_BUCKET_NAME = config.DEFAULT_BUCKET_NAME;

const GOOGLE_CLOUD_PROJECT_ID = config.GOOGLE_CLOUD_PROJECT_ID;
const GOOGLE_CLOUD_KEYFILE = keyFile;

// const storage = new Storage({
//   projectId: GOOGLE_CLOUD_PROJECT_ID,
//   keyFilename: GOOGLE_CLOUD_KEYFILE,
// });

const storage = new Storage();

class DownloaderUtil {

    static async downloadFromGCP(p_file_loc){
        return new Promise(function(resolve , reject ){
            try {
              const bucketName = DEFAULT_BUCKET_NAME;
              const bucket = storage.bucket(bucketName);
              const file = bucket.file(p_file_loc);
            
              const CONFIG = {
                action: "read",
                expires: "03-01-3000",
              };
            
              file.getSignedUrl(CONFIG,function(err, url) {
                if (err) {
                    console.log("[file_download]:error downloaded file from url: "+p_file_loc);
                    resolve("");
                }else {
                    resolve(url)
                }
              })
            } catch (error) {
              console.log('Error download.utils:',error)
                reject(error)
            }
        });
    }
}

module.exports.DownloaderUtil = DownloaderUtil

