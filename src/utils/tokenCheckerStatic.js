const { errorCatch, errorResponse } = require ('./response.util')
var common = require('../utils/common.util');
var config = common.config();

module.exports = (req,res,next) => {
  try {
    let checkRevoked = true;
    const { authorization } = req.headers
    if (!authorization || authorization==undefined) {errorResponse(res,401,16008)}
    else{
      const [authType, token] = authorization.trim().split(' ')
      if (authType !== 'Bearer') errorResponse(res,401,16007)
    
      // console.log('token nya bray :',config.token.logol)

     
        if (config.token.logol == token) {
          // req.decoded = decodedToken;
          next();
        } else {
          errorResponse(res,401,10004)
        }
      
      }  
  } catch (error) {
    errorCatch(res,error)
  }
}