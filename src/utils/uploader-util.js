const {Storage} = require('@google-cloud/storage');
var common = require('./common.util')
var config = common.config();

const keyFile = './src/config/cloudstorage.serviceaccount.json'

const DEFAULT_BUCKET_NAME = config.DEFAULT_BUCKET_NAME;

const GOOGLE_CLOUD_PROJECT_ID = config.GOOGLE_CLOUD_PROJECT_ID;
const GOOGLE_CLOUD_KEYFILE = keyFile;

const storage = new Storage({
  projectId: GOOGLE_CLOUD_PROJECT_ID,
  keyFilename: GOOGLE_CLOUD_KEYFILE,
});


/**
   * Get public URL of a file.
   * @param {string} bucketName
   * @param {string} fileName
   * @return {string}
   */
exports.getPublicUrl = (bucketName, destination) => `https://storage.googleapis.com/${bucketName}/${destination}`;

/**
 * Middleware for uploading file to GCS.
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @return {*}
 */
exports.sendUploadToGCS = async (req, res, next) => {
  if (!req.file) {
    return next();
  }
  
  const bucketName = req.body.bucketName || DEFAULT_BUCKET_NAME;
  const bucket = storage.bucket(bucketName);
  
  const destination = req.destination
  const gcsFileName = `${Date.now()}-${destination}`;
  const file = bucket.file(destination);
  console.log(destination)
  

  const stream = file.createWriteStream({
    
    public: true,
    // gzip: true,
    metadata: {
      contentType: req.file.mimetype,
      'Cache-Control':'no-cache, maxAge:0',
    }
    
    // maxAge=0
    // CacheControl:"public, max-age=1"
  });

  const CONFIG = {
    action: "read",
    expires: "03-01-3000",
};
// file.getSignedUrl(CONFIG)

  stream.on('error', (err) => {
    req.file.cloudStorageError = err;
    next(err);
  });

  stream.on('finish', () => {
    // req.file.cloudStorageObject = gcsFileName;
    req.file.cloudStorageObject = destination;
    file.getSignedUrl(CONFIG,function(err, url) {
      if (err) {
        req.file.cloudStorageError = err;
    next(err);
      } else {
        console.log(url)
        req.file.gcsUrl = url
        next();
      }
    })

    return file.makePublic()
      .then(() => {
        req.file.gcsUrl = this.getPublicUrl(bucketName,destination);
        // res.send(req.file.gcsUrl)
        next();
      });
  });

  stream.end(req.file.buffer);
};