const path = require('path'); 
require('dotenv').config({ path: path.join(__dirname, '.env') });
var env = require('../config/env.json');

exports.config = function() {
  var node_env = process.env.NODE_ENV || 'DEV';
  return env[node_env];
};