var validator = require('validator');

var Obj = {
    escapeObject:function (obj) {
        let result = obj;
        for (let key in result) {
            if (typeof result[key] === 'string') {
                result[key] = validator.escape(result[key]);
            } else if (typeof result[key] === 'object') {
                result[key] = Obj.escapeObject(result[key]);
            }
        }
        return result;
    },
    unescapeObject: function (obj) {
        let result = obj;
        for (let key in result) {
            if (typeof result[key] === 'string') {
                result[key] = validator.unescape(result[key]);
            } else if (typeof result[key] === 'object') {
                result[key] = Obj.unescapeObject(result[key]);
            }
        }
        return result;
    },
}

module.exports = Obj;