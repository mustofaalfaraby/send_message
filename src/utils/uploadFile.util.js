
const fs = require("fs");

class UploadFile {

    static async executeFileUpload(req) {

        return new Promise(function (resolve, reject) {
            const { image,imagename,location } = req
            
            // let buff = new Buffer(image, 'base64');
            // let buff = Buffer.from(image, 'base64')
            let buff = Buffer.from(image, 'base64')

            if (!fs.existsSync(location)) {
                fs.mkdirSync(location);
            }

            fs.writeFileSync(location + imagename
                , buff);

            resolve(true)
        });
    }
}

module.exports.uploadFile = UploadFile

