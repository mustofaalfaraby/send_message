var moment = require('moment');

module.exports = {
    currentDate: (() => {
        return moment().tz("Asia/Jakarta").format('YYYY-MM-DD hh:mm:ss');
    })
}